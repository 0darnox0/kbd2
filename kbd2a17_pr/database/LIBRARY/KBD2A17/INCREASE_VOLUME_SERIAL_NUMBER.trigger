-- <?xml version = '1.0' encoding = 'UTF-8'?>
-- <trigger xmlns="http://xmlns.oracle.com/jdeveloper/1211/offlinedb">
--   <name>INCREASE_VOLUME_SERIAL_NUMBER</name>
--   <enabled>true</enabled>
--   <properties>
--     <entry>
--       <key>OfflineDBConstants.IMPORT_SOURCE_CONNECTION</key>
--       <value class="java.lang.String">kbd</value>
--     </entry>
--     <entry>
--       <key>OfflineDBConstants.IMPORT_SOURCE_ID</key>
--       <value class="oracle.javatools.db.ReferenceID">
--         <name>INCREASE_VOLUME_SERIAL_NUMBER</name>
--         <identifier class="java.math.BigDecimal">86488</identifier>
--         <schemaName>KBD2A17</schemaName>
--         <type>TRIGGER</type>
--       </value>
--     </entry>
--   </properties>
--   <statementLevel>true</statementLevel>
-- </trigger>

CREATE OR REPLACE
TRIGGER increase_volume_serial_number
  BEFORE INSERT ON VOLUMES 
  FOR EACH ROW
  DECLARE
    max_id number;
BEGIN
    SELECT NVL(MAX(VOLUME_SERIAL_NUMBER), -1) INTO max_id FROM VOLUMES WHERE ISBN=:NEW.ISBN;
    :NEW.VOLUME_SERIAL_NUMBER := max_id + 1;
END;
/
