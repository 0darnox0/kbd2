-- <?xml version = '1.0' encoding = 'UTF-8'?>
-- <trigger xmlns="http://xmlns.oracle.com/jdeveloper/1211/offlinedb">
--   <name>CAN_BE_BOROWED</name>
--   <enabled>true</enabled>
--   <properties>
--     <entry>
--       <key>OfflineDBConstants.IMPORT_SOURCE_CONNECTION</key>
--       <value class="java.lang.String">kbd</value>
--     </entry>
--     <entry>
--       <key>OfflineDBConstants.IMPORT_SOURCE_ID</key>
--       <value class="oracle.javatools.db.ReferenceID">
--         <name>CAN_BE_BOROWED</name>
--         <identifier class="java.math.BigDecimal">86696</identifier>
--         <schemaName>KBD2A17</schemaName>
--         <type>TRIGGER</type>
--       </value>
--     </entry>
--   </properties>
--   <statementLevel>true</statementLevel>
-- </trigger>

CREATE OR REPLACE
TRIGGER can_be_borowed
BEFORE INSERT ON borrowings
FOR EACH ROW
DECLARE
    is_book_borrowed number;
    max_date date;
BEGIN
  SELECT TO_DATE('31.12.9999 23:59:59', 'dd.mm.yyyy hh24:mi:ss') INTO max_date FROM dual;
  
  SELECT COUNT(*) INTO is_book_borrowed
  FROM borrowings
  WHERE isbn = :new.isbn
  AND volume_serial_number = :new.volume_serial_number
  AND BORROWING_DATE <= NVL(:new.RETURN_DATE, max_date) 
  AND NVL(RETURN_DATE, max_date) >= :new.BORROWING_DATE;

  
  IF is_book_borrowed > 0
  THEN
    RAISE_APPLICATION_ERROR(-20500, 'This book is already borrowed!');
  END IF;
END;
/
