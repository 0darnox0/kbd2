-- <?xml version = '1.0' encoding = 'UTF-8'?>
-- <trigger xmlns="http://xmlns.oracle.com/jdeveloper/1211/offlinedb">
--   <name>DELIMIT_BORROWINGS</name>
--   <enabled>true</enabled>
--   <properties>
--     <entry>
--       <key>OfflineDBConstants.IMPORT_SOURCE_CONNECTION</key>
--       <value class="java.lang.String">kbd</value>
--     </entry>
--     <entry>
--       <key>OfflineDBConstants.IMPORT_SOURCE_ID</key>
--       <value class="oracle.javatools.db.ReferenceID">
--         <name>DELIMIT_BORROWINGS</name>
--         <identifier class="java.math.BigDecimal">86461</identifier>
--         <schemaName>KBD2A17</schemaName>
--         <type>TRIGGER</type>
--       </value>
--     </entry>
--   </properties>
--   <statementLevel>true</statementLevel>
-- </trigger>

CREATE OR REPLACE
TRIGGER delimit_borrowings 
  BEFORE INSERT ON borrowings 
  FOR EACH ROW
  DECLARE
    count_borrowings number;
BEGIN
  SELECT COUNT(*) INTO count_borrowings 
  FROM borrowings
  WHERE user_id = :new.user_id
  AND RETURN_DATE = NULL;

  IF count_borrowings > 10
    THEN
      RAISE_APPLICATION_ERROR(-20500, 'You cannot borrow more books!');
  END IF;
END;
/
